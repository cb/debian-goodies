.\" checkrestart.8 - provide a list of processes that need to be restarted
.\" Copyright (C) 2006-2014 Javier Fernandez-Sanguino
.\" Copyright (C) 2022 Richard Lewis
.\"
.\" This program is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 2, or (at your option)
.\" any later version.
.\"
.\" On Debian systems, a copy of the GNU General Public License version 2
.\" can be found in /usr/share/common-licenses/GPL-2.
.TH checkrestart 8 "December 19 2006" "debian\-goodies" "debian\-goodies"
.SH NAME
checkrestart \- list processes that need to be restarted after an upgrade

.SH SYNOPSIS
.B checkrestart \fR[\fP \fIOPTIONS\fP \fR]\fP

.SH DESCRIPTION
.B checkrestart
finds processes that are using files that have been deleted.
.PP
This is particularly important after security upgrades because many
debian packages do not restart processes after an upgrade: files that
were used by processes started before the upgrade will remain in
memory until the process is restarted: the processes is likely to be
vulnerable until it is restarted.

.PP
Consequently, checkrestart is sometimes used as an audit tool to find
services that need to be restarted after security
upgrades. Administrators should not, however, rely on its output alone
(see \fBBUGS\fP below).

.PP
checkrestart needs to run as root in order to obtain a complete list
of deleted files that are in use. If run as a non-root user the output
will be incomplete: programs started by other users are likely to be
omitted.

.PP
checkrestart will also warn you if other packages have indicated that
a reboot is required.


.SH OUTPUT
checkrestart will output:
.IP \(bu 2
Whether any packages have indicated that the system needs rebooting,
and if so, which packages have done so. This relies on the packages
adding themselves to \fB/run/reboot\-required.pkgs\fP and creating
\fB/run/reboot\-required\fP.

.IP \(bu
The number of processes that need restarting.

.IP \(bu
Which processes are using deleted files. Processes are grouped by the
systemd unit that started them or the debian package that provided
them. The \fB\-\-exclude\fP option can be used to exclude processes from
the results.

.IP \(bu
If the \fB\-f\fP option was given then the deleted files used by each
process will also be listed. The \fB\-\-exclude\fP option can be used
to ignore the use of individual files.

.IP \(bu
Which commands to run to restart packages. These will be commands to
restart systemd units or initscripts. If the system has departed from
the Debian default and is not running systemd with the
.BR cgroups (7)
feature then the commands should be considered 'suggestions' only: it
is not possible to reliably determine which initscript started any
given process (the \fB\-\-exclude\fP option can be used to control
such suggestions). Where commands are potentially disruptive (e.g.,
restarting \fBsystemd-logind\fP may result in users being immediately
logged out) they can be marked with a `\fBCAUTION\fP' note \- the
\fB\-\-exclude\fP option can control which commands this applies to.

.PP
If the \fB\-m\fP option is given then the output is tab-separated and
machine-readable (see the description of that option below). If the
\fB\-t\fP option is given then the output is restricted to one line.

.SH OPTIONS
.TP
\fB\-h\fP, \fB\-\-help\fP
Show the program help and exit.

.TP
\fB\-f\fP, \fB\-\-show-files\fP
List the deleted files and which program is using them. The list
excludes anything excluded by the \fB\-\-exclude\fP) option. Without
this only the name of the program using the deleted files is reported.

.TP
\fB\-v\fP, \fB\-\-verbose\fP
Generate detailed output. This turns on the \fB\-f\fP option and also
indicates why any exclusions were made.

.TP
\fB\-d\fP, \fB\-\-debug\fP
Include debugging details in output. This is intended for
investigating bugs and turns on the \fB\-v\fP option.

.TP
\fB\-t\fP, \fB\-\-terse\fP
Generate just one line of output: this is suitable for monitoring
tools such as Nagios (see \fBEXIT STATUS\fP).

.TP
\fB-m\fP, \fB\-\-machine\fP
Generate machine readable output. Each line is a tab-separated list.
.IP
First the output shows what needs to be restarted in the form:
.br
`\fITYPE\fP  \fIsource\fP  \fIpid\fP  \fIprogram\fP  \fIexe\fP  \fB[\fP\fIcmdline\fP\fB]\fP \fIdeleted\fP \fItype\fP'

.IP
Here the
.IR "exe"
is what is actually running. For scripts this will be the interpreter,
but if it can be determined, the name of the script will be reported
as the
.I  programme
and the
.I cmdline
as reported by
\fB/proc/\fP\fIpid\fP\fB/cmdline\fP will be shown.
This can be manipulated by the programme itself, unlike the
.I exe
which is from \fB/proc/\fP\fIpid\fP\fB/exe\fP.  \fItype\fP will be
\fBProgram\fP or \fBScript\fP. The \fIdeleted\fP field is usually
blank but will be \fBDeleted\fP if the \fIexe\fP itself is deleted
(this does not work for scripts). The \fIsource\fP indicates which
systemd unit or Debian package is responsible for the \fIprogramme\fP.


.IP
The \fITYPE\fP
is one of:
.RS
.TP
\fBSYSTEMD\fP,
if the
.I program
was started by the systemd unit named in
.IR source .
Restarting that unit will restart the program.
These lines will only be produced if systemd is being used and version
2 of the
.BR cgroups (7)
feature is in use (this is the default in Debian).

.IP
For example,
.br
`\fBSYSTEMD foo.service 614 /usr/bin/foo /usr/bin/python3  ['python' 'foo']  Script\fP'
.br
means that restarting the
.B foo.service
unit will restart the python script
.B foo
with pid 614.

.TP
\fBPACKAGE_SYSTEMD\fP,
if
.I program
is part of the package named in
.I source
and the package also ships a systemd unit: restarting that unit may
restart the programe, but this is only a suggestion \- it is not
guaranteed that it will work. You can control which
units are suggested using the \fB\-x\fP option. These lines will only
be produced if
.BR systemd (1)
is in use without version 2 of the
.BR cgroups (7)
feature.

.IP
For example,
`\fBPACKAGE_SYSTEMD gdm3 206 /usr/libexec/gdm-session-worker ...\fP'
means that the
.B gdm3
package provides
.B gdm-session-worker
and some unit in that package that may restart that
programme.

.TP
\fBSERVICE\fP,
if
.I program
is part of a package named in
.IR source
that ships an initscript. Restarting the initscript may therefore
restart the program, but this is not known for sure (there is no way
to tell which initscipt started a program). You will only see these
lines if you do not run systemd. These lines are produced if you run
systemd without cgroups version 2: if systemd is in use you will then
get \fBPACKAGE_SYSTEMD\fP in preference to \fBSERVICE\fP lines when
packages provide both units and initscripts. If systemd is not in use
you will only get \fBSERVICE\fP lines.

.TP
\fBOTHER\fP,
if
.I program
is in none of the above categories. These programs, whether or not
they are from packages, still need restarting but the user will need
to do so by hand. These lines can be produced whether systemd is in
use or not. A \fIprogram\fP not in any package is treated as if it was
in a package named `\fBUnpackaged:\fP \fIprogram\fP', the part after
the colon being the name, not the path, of the \fIprogram\fP. (If
\fB\-p\fP is in use then unpackaged programs are excluded).
.IP
For example,
`\fBOTHER emacs-gtk 206706 /usr/bin/emacs-gtk\fP...' could be emitted if
.BR emacs (1)
is using deleted files.
.RE
.RE

.IP
The next set of lines show the commands that will restart
.IR program s
in the
.B SYSTEMD
lines, and which may restart
.IR program s
in the
.B PACKAGE_SYSTEMD
or
.B SERVICE
lines. These look like:
.RS
.RS
.TP
\fBSYSTEMD_COMMAND systemctl restart\fP \fIunit\fP
produced by any
.B SYSTEMD
lines

.TP
\fBSYSTEMD_COMMAND systemctl restart\fP \fIunit\fP \fB# suggested - from package\fP \fIpackage\fP
produced by any
.B PACKAGE_SYSTEMD
lines: the comment shows which package contains the suggested
unit. If a package provides multiple units they will all be
listed on separate lines. You can use the '\fB\-\-dont\-suggest\-unit\fP' option to
remove these lines.

.TP
\fBSERVICE_COMMAND service restart\fP \fIinitscript\fP \fB# suggested - from package\fP \fIpackage\fP
which relate to
.B SERVICE
lines: the comment shows which package contains the suggested
initscript. If a package provides multiple initscripts they will all be
listed on separate lines. You can use the `\fB\-\-dont\-suggest\-initscript\fP' option to
remove these lines.


.TP
\fB# CAUTION: ...\fP
Lines prefixed by a this comment are potentially disruptive: The
command does need to be run, but doing to may cause issues (such as
terminating your whole gnome session): see the
`\fB\-\-dangerous\-unit\fP' and `\fB\-\-dangerous\-initscript\fP'
options.

.RE
.RE

.IP
If the
.B \-f
option is also present the output will include a line for each deleted
files, these are tab-separated lines that look like:
`\fBfile\fP \fIpath\fP \fIpid\fP \fIexe\fP \fB[\fP\fIcmdline\fP\fB]\fP \fIdeleted\fP \fItype\fP'
showing the path to the file and then details of the process
using it (fields are explained above).

.IP
If the
.B \-v
option is also present there will include lines explaining anything
excluded. These are also tab separated and are similar to the fields
used above.

.TP
\fB\-x\fP, \fB\-\-exclude\fP [\fITYPE\fP\fB:\fP]\fIREGEXP\fP
Exclude all things of the given
.I TYPE
that match
.IR REGEXP .
This option can be used multiple times to make multiple exclusions,
and anything added is combined with entries from the various
configuration files (see the
.B CONFIGURATION FILES
section below) and from any files loaded with \fB\-b\fP.  You may need
to insert single quotes around the whole argument if it contains
characters such as
.B $
that your shell treats specially.  (e.g.,
`\fBcheckrestart -x 'file:(\e.sh$|foo)\fP')
The word
.I TYPE
can be one of the following:
.RS
.RS

.TP
\fBpackage\fP, which excludes programs from the debian package whose name matches \fIREGEXP\fP.
For example, to exclude
.B /usr/bin/sshd
you can use: `\fB\-x package:^openssh\-server\e$\fP'. Programs not from any
package are treated as if they were provided by a package called
`\fBUnpackaged:\fP \fIprogram\fP', so you can ignore them in the same way as
packaged programs (for example,`\fB\-x package:^Unpackaged:\esfoo\fP' will ignore
a locally installed \fB/opt/foo\fP).

.TP
\fBunit\fP, which excudes all programs started by the systemd unit whose name matches \fIREGEXP\fP.
For example, under systemd, you can exclude
.B /usr/sbin/exim4
using \fB\-x unit:^exim4\e.service$ \fP. This will work even if the
package only provided an initscript (systemd will generate a virtual
unit using
.BR systemd-sysv-generator (1)).

.IP
This option requires that version 2 of the cgroups feature is in use (which
is the default in Debian). It has no effect if
.BR systemd (1)
is not being used.

.TP
\fBprogram\fP, which excludes all programs whose path matches \fIREGEXP\fP.
For example, use
`\fB\-x program:^/usr/local/bin/\fP' to exclude a whole directory.

.TP
\fBpid\fP,
which excludes the process with a process id (pid) matching \fIREGEXP\fP.
For example,
.B \-x pid:^1\e$
excludes the init. Note that
.I REGEXP
is still a regular expression so use of
.B ^
and
.B $
are recommended to avoid excluding too much.

.TP
\fBfile\fP, which excludes processes using deleted files that match \fIREGEXP\fP.
For example,
`\fB\-x file:libz\e.so\e..+\fP'
will exclude everything using (any version of) the libz library.
If no
.I TYPE
is specified then it is the same as using
.BR file .

.TP
\fBdangerous\-unit\fP
which does not exclude anything from being reported as using deleted
files, but instead marks any commands involving that unit with a
\fBCAUTION\fP warning. This is intended to be used when restarting a
unit is potentially disruptive. For example, restarting gdm3.service
will terminate the entire gnome session. It still needs to be done to
ensure gnome stops using deleted files, but the user will want to pick
their moment.


.TP
\fBdangerous\-initscript\fP
which does not exclude anything from being reported as using deleted
files, but instead marks any commands involving that initscript with a
\fBCAUTION\fP warning. This is intended to be used when restarting an
initscript is potentially disruptive. For example, restarting gdm3
will terminate the entire gnome session. It still needs to be done to
ensure gnome stops using deleted files, but the user will want to pick
their moment.


.TP
.BR suggested-unit ,
which does not exclude anything from being reported as using deleted
files, but instead stops
.B checkrestart
from suggesting that a systemd service matching
.I REGEXP
can restart any programme. This is only relevant to systems running
systemd and not using cgroups version 2. On such systems, if a process
is found to be started by a unit then that unit will always be
suggested as the way to restart the process. But when a process is not
started by a service,
.B checkrestart
looks in the package providing the unit and suggests all units as
possible ways to restart the process. For example,
/usr/libexec/gdm-session-worker from the gdm3 package is started by
gnome but does not appear in the cgroup created by the
gdm3.service. However, gdm3.service will still be suggested as a way
to restart this process. Setting
.B -x done-suggest:^gdm3\.service\e$
will prevent that suggestion being made.

.TP
\fBsuggested\-initscript\fP,
which does not exclude anything from being reported as using deleted
files, but instead stops
.B checkrestart
from suggesting that an initscript matching
.IR REGEXP
can restart a programme. If systemd is not being used, or a process
was not found in a systemd cgroup, then
.B checkrestart
suggests that all initscripts from the relevant package can restart
the process unless their path matches a \fBsuggested\-initscript\fP
.IR REGEXP .
For example, the default settings include
.B -x 'initscript:\e.sh$'
so that
.B /etc/init.d/hwclock.sh
will never be (incorrectly) suggested as a way to restart
.B /sbin/getty
even though both are from the
.B util-linux
package.
.IP
This is mostly useful for non-systemd systems since on
systemd, the cgroups mechanism will be used to find exactly which unit
started each process.
.RE
.RE

.TP
\fB\-i\fP \fIREGEXP\fP, \fB\-\-exclude\-package\fP \fIREGEXP\fP
Is the same as
.B \-x package:\fIREGEXP\fP

.TP
\fB\-\-exclude\-unit\fP \fIREGEXP\fP
Is the same as
.B \-x unit:\fIREGEXP\fP

.TP
\fB\-\-exclude\-program\fP \fIREGEXP\fP
Is the same as
.B \-x program:\fIREGEXP\fP

.TP
\fB\-e\fP \fIPID\fP, \fB\-\-exclude\-pid\fP \fIREGEXP\fP
Is the same as
.B \-x pid:\fIREGEXP\fP

.TP
\fB\-\-exclude\-file\fP \fIREGEXP\fP
Is the same as
.B \-x file:\fIREGEXP\fP

.TP
\fB\-\-dont\-suggest\-unit\fP \fIREGEXP\fP
Is the same as
.B \-x suggested:\fIREGEXP\fP

.TP
\fB\-\-dangerous\-unit\fP \fIREGEXP\fP
Is the same as
.B \-x dangerous\-unit:\fIREGEXP\fP

.TP
\fB\-\-dangerous\-initscript\fP \fIREGEXP\fP
Is the same as
.B \-x dangerous\-initscript:\fIREGEXP\fP



.TP
\fB\-\-dont\-suggest\-initscript\fP \fIREGEXP\fP
Is the same as
.B \-x initscript:\fIREGEXP\fP

.TP
\fB\-a\fP, \fB\-\-all\fP
Prevents the
.B exclude.conf
and
.B local-exclude.conf
files from being read and removes the effect of all
.B \-\-exclude*
and
.B \-b
arguments given earlier on the command line.  This means nothing will
be excluded. This can then be followed by further uses of those
arguments to rebuild the 'exclusion' settings exactly how you want
them.  It does not reset anything added through the
.B \-\-dont\-suggest\-*
or
.B \-\-dangerous\-*
options, or prevent the
.B suggestions.conf
or
.B local-suggestions.conf
files from being read (see the
.B CONFIGURATION FILES
section below).

.TP
\fB\-p\fP, \fB\-\-package\fP
Only report things that belong to a package. This applies to files,
programmes, initscripts and units: anything not from Debian packages
is ignored. (If you want to exclude an individual package, see the
\fB\-x\fP option.)


.TP
\fB\-n\fP, \fB\-\-no\-lsof\fP
Do not use
.BR lsof (8)
to find deleted files. lsof may be slow if there are a large number of
open files, and this option will cause
.B checkrestart
to use an alternative mechanism for finding deleted files.  If
.BR lsof (8)
is not installed the alternative mechanism with be used automatically.

.TP
\fB\-b\fP \fIFILE\fP, \fB\-\-blocklistfile\fP \fIFILE\fP
Any deleted files matching patterns (Python regular expressions) in
.I file
will be ignored. Lines in
.I file
that are blank or start with
.RB '#'
are skipped. This option can be used multiple times.

.SH CONFIGURATION FILES
If they exist, the files
.BR /etc/checkrestart/local-exclude.conf ,
.BR /etc/checkrestart/exclude.conf ,
.BR /etc/checkrestart/local-suggestions.conf ,
and
.BR /etc/checkrestart/suggestions.conf
files are read to provide defaults for the \fB\-x\fP option. Blank
lines and lines starting with a \fB#\fP are ignored, and any other
line is passed to the \fB\-x\fP option.

.PP
The idea is that Debian provides defaults for \fB\-\-exclude\-*\fP in
\fBexclude.conf\fP, defaults for (for and \fB\-\-dont\-suggest\-*\fP
and \fB\-\-dangerous\-*\fP in \fBsuggestions.conf\fP, and you can add
your own via the two \fBlocal-*.conf\fP files.

.PP
However you can actually put anything understood by \fB\-x\fP into
either file: The only difference is that the \fB\-a\fP option prevents
the two \fBexclude.conf\fP files from being read, but does not affect
the two \fBsuggestions.conf\fP files at all.

.PP
The \fB\-b\fP option also allows you to add another file with the same
syntax, and again this can contain anything understood by \fB\-x\fP.
.


.SH EXIT STATUS

The program will exit with error status 1 when there are deleted open
files, 0 when there are none (after filtering out anything excluded
through the \fB\-\-exclude\fP options), and 3 if rheee are
errors. This provides compatibility with automated monitoring tools
such as Nagios (for which you may wish to use the \fB\-t\fP option to
get a single line of output).


.SH EXAMPLES
Running as a normal user
  \fB$ checkrestart\fP
  WARNING: This program should be run as root: information will be incomplete
  1 non-ignored program(s) or unit(s) need restarting (see checkrestart(8))

  The following are using deleted files but there is no suggested way to restart them:
  emacs-gtk:
          Program /usr/bin/emacs-gtk (PID: 655075, CMDLINE: 'emacs -nw')
.PP
Running as root:
  \fB# checkrestart\fP
  3 non-ignored program(s) or unit(s) need restarting (see checkrestart(8))

  The following systemd units started programmes that are using deleted files:
  cron.service:
    Program /usr/sbin/cron (PID: 626, CMDLINE: '/usr/sbin/cron -f')
  dbus.service:
    Program /usr/bin/dbus-daemon (PID: 627, CMDLINE: '/usr/bin/dbus-daemon --system --address=systemd: --nofork --nopidfile --systemd-activation --syslog-only')

  The following are using deleted files but there is no suggested way to restart them:
  emacs-gtk:
    Program /usr/bin/emacs-gtk (PID: 655075, CMDLINE: 'emacs -nw')


  Systemd commands:
  systemctl restart cron.service
  # CAUTION: systemctl restart dbus.service

.PP
Excluding things from the results, and listing files with \fB\-f

  # checkrestart \-x 'unit:^dbus\.service$' \e
.br
                 \-x 'program:^/usr/bin/(emacs|vim)' \e
.br
                 \-\-dangerous-unit '^(ana)?cron\.service$' \e
.br
                 \-\-show\-files
\fP
  1 non-ignored program(s) or unit(s) need restarting (see checkrestart(8))

  The following systemd units started programmes that are using deleted files:
  cron.service:
    Program /usr/sbin/cron (PID: 626, CMDLINE: '/usr/sbin/cron -f')
      /lib/x86_64-linux-gnu/ld-2.31.so

  Systemd commands:
  # CAUTION: systemctl restart cron.service


.SH BUGS
.PP
.B checkrestart
will not detect if a script was itself deleted.

.B checkrestart
will be generate false positives if non-deleted files or programs have
names ending in `\fB(deleted)\fP'.

.B checkrestart
may report the wrong name, if a program is actually a script. Common
scripts will be detected.

.B checkrestart
will believe whatever a process writes into \fB/proc/pid/cmdline\fP,
so may report the wrong program name. (See
.BR proc (5)).

.B checkrestart
can only report that processes using deleted files, and assumes that
these always indicate that a restart is needed: it cannot tell whether
deleted files are expected or whether they should be flagged (other
than what you tell it via the various \fB\-x\fP options).


.PP
If you find a bug, please provide the following information when
submitting a bug report against the \fBcheckrestart\fP package (using
.BR reportbug (1)):

.IP \(bu 2
The output from
.B checkrestart --debug
(include any other options that trigger the bug)

.IP \(bu
The output from running the following command as root:
.B lsof | grep -E 'delete|DEL|path inode'

.SH  SEE ALSO
.TP
.BR needrestart (8)
is a similar tool to
.BR checkrestart .
It runs when new versions of debian packages are installed, whereas
.B checkrestart
can run at any time.  The two can be used together, and may give
different results \- although if
.B needrestart
finds something that
.B checkrestart
does not then that is a bug in
.BR checkrestart:
please report such instances using
.BR reportbug (1).

.IP
.B needrestart
is also intended to do the restarting automatically, whereas
.B checkrestart
is for reporting and will leave taking action to the humans.

.TP
.BR lsof (8),
is a generic tool for investigating which files are in use.
.B checkrestart
uses this (unless the
.B \-n
option is given)

.TP
.BR pmap (1),
is another tool for inspecting which files are loaded. The
.B \-n
option uses this.

.TP
.BR proc (5),
is a pseudo filesystem that contains information about running processes.

.TP
.BR cgroups (7),
explains the control groups mechanism used by
.BR systemd (1)
to keep related processes together.
.B checkrestart
assumes version 2 is in use if systemd is running.

.TP
.BR systemctl (1),
is the generic tool to restart services under
.BR systemd (1).

.TP
.BR service (1)
is the generic tool to restart services if
.B systemd
is not in use.


.SH AUTHOR

.B checkrestart
was written by Matt Zimmerman for the Debian GNU/Linux distribution. It was
later improved by Javier Fernandez-Sanguino with contributions from many
different users and developers of the Debian GNU/Linux distribution.

.SH COPYRIGHT AND LICENSE

Copyright (C) 2001 Matt Zimmerman <mdz@debian.org>
.br
Copyright (C) 2007-2020 Javier Fernandez-Sanguino <jfs@debian.org>
.br
Copyright (C) 2013-2020 Axel Beckert
.br
Copyright (C) 2022 Richard Lewis

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

On Debian systems, a copy of the GNU General Public License version 2
can be found in /usr/share/common-licenses/GPL-2.
